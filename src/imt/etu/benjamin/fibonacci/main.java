package imt.etu.benjamin.fibonacci;

public class main {

    // complexité e(ç)
    private static int fibonacci(int height) {
        if (height == 0) return 0;
        if (height <= 1) return 1;
        else return fibonacci(height - 1) + fibonacci(height - 2);
    }

    public static void main(String[] args) {
        // 0,1,1,2,3,5,8,13,21,34,55,89,144,233,...
        int res = 0;
        for (int i = 0; i < 11; i+=1)
            res = fibonacci(i);

    }
}